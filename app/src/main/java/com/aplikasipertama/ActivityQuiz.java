package com.aplikasipertama;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;


public class ActivityQuiz extends AppCompatActivity {

    EditText ed1;
    TextView tv1,tv2,tv3;
    RadioButton a,b,c,d;
    Button back;
    ImageView iv1,iv2;
    Button bt;
    RadioGroup rg;
    int q,s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_quiz);
        ed1=(EditText) findViewById(R.id.name);
        tv1=(TextView)findViewById(R.id.ques);
        tv2=(TextView)findViewById(R.id.response);
        tv3=(TextView)findViewById(R.id.score);
        iv1=(ImageView)findViewById(R.id.smile);
        iv2=(ImageView)findViewById(R.id.sad);
        back=(Button)findViewById(R.id.back);
        rg=(RadioGroup)findViewById(R.id.optionGroup);
        a=(RadioButton)findViewById(R.id.option1);
        b=(RadioButton)findViewById(R.id.option2);
        c=(RadioButton)findViewById(R.id.option3);
        d=(RadioButton)findViewById(R.id.option4);
        bt=(Button)findViewById(R.id.next);
        q=0;
        s=0;

    }
    public void quiz(View v){
        switch (q){
            case 0:
            {
                ed1.setVisibility(View.INVISIBLE);
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                tv2.setText("");
                tv3.setText("");
                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s=0;

                tv1.setText("1. 12 + 10 = ?");
                a.setText("A. 20");
                b.setText("B. 21");
                c.setText("C. 23");
                d.setText("D. 22");
                q=1;
                break;
            }
            case 1:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("2. 28 - 7 = ?");
                a.setText("A. 19");
                b.setText("B. 20");
                c.setText("C. 21");
                d.setText("D. 22");

                if (d.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!d.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 2:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("3. 6 X 9 = ?");
                a.setText("A. 54");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (a.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!a.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 3:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("4. 1 + 1 x 0 = ?");
                a.setText("A. 0");
                b.setText("B. 1");
                c.setText("C. 2");
                d.setText("D. 3");
                if (b.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!b.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 4:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("5. 9 x 6 = ?");
                a.setText("A. 54");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (c.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!c.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 5:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("6. 8 bagi 2 = ?");
                a.setText("A. 4");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (a.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!a.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("7. 6 X 9 = ?");
                a.setText("A. 54");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (c.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!c.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("8. 6 X 9 = ?");
                a.setText("A. 54");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (c.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!c.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("9. 6 X 9 = ?");
                a.setText("A. 54");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (c.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);

                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!c.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("10. 72 : 8 = ?");
                a.setText("A. 8");
                b.setText("B. 7");
                c.setText("C. 9");
                d.setText("D. 6");
                if (a.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!a.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                q=10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 10:
            {
                ed1.setVisibility(View.INVISIBLE);
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                bt.setText("Finish");
                if (c.isChecked())
                {
                    tv2.setText("Right Answer");
                    s=s+50;
                    iv1.setVisibility(View.VISIBLE);
                    iv1.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv1.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                else if (!a.isChecked()&& !b.isChecked()&& !c.isChecked()&&!d.isChecked())
                {
                    tv2.setText("Tidak Pilih Jawaban");
                    s=s;
                }
                else if (!c.isChecked())
                {
                    tv2.setText("Wrong Answer");
                    s=s-10;
                    iv2.setVisibility(View.VISIBLE);
                    iv2.postOnAnimationDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv2.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }

                tv3.setText(ed1.getText()+"'s final score is "+s);
                bt.setText("Restart");
                q=0;
                back.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View v)
                                            {
                                                Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                                finish();
                                                System.exit(0);
                                            }
                                        }
                );
                break;
            }
        }
    }
}