package com.aplikasipertama;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    //Inisialisasi Pertama di Java
    FrameLayout btnprofil;
    FrameLayout btncity;
    FrameLayout btnedc;
    FrameLayout btnfmly;
    FrameLayout btnquiz;
    FrameLayout btnclose;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inisialisasi Kedua di Java
        btnprofil = (FrameLayout) findViewById(R.id.btn_profile);
        btncity = (FrameLayout) findViewById(R.id.btn_mycity);
        btnedc = (FrameLayout)findViewById(R.id.btn_myeducation);
        btnfmly = (FrameLayout) findViewById(R.id.btn_myfamily);
        btnquiz = (FrameLayout) findViewById(R.id.btn_quiz);
        btnclose = (FrameLayout) findViewById(R.id.btn_close);
        long lastPress;
        Toast backpressToast;

        //Button Profil Ketika DiKlik
        btnprofil.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View v)
                                         {
                                             Toast.makeText(getApplicationContext(), "Profil Telah Dipilih", Toast.LENGTH_SHORT).show();
                                             Intent beach = new Intent(MainActivity.this, ActivityProfile.class);
                                             startActivity(beach);
                                         }
                                     }
        );

        //Button MyCity Ketika DiKlik
        btncity.setOnClickListener(new View.OnClickListener()
                                   {
                                       @Override
                                       public void onClick(View v)
                                       {
                                           Toast.makeText(getApplicationContext(), "MyCity Telah Dipilih", Toast.LENGTH_SHORT).show();
                                           Intent beach = new Intent(MainActivity.this, ActivityCity.class);
                                           startActivity(beach);
                                       }
                                   }
        );

        //Button MyEducation Ketika DiKlik
        btnedc.setOnClickListener(new View.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v)
                                      {
                                          Toast.makeText(getApplicationContext(), "MyEducation Telah Dipilih", Toast.LENGTH_SHORT).show();
                                          Intent beach = new Intent(MainActivity.this, ActivityEducation.class);
                                          startActivity(beach);
                                      }
                                  }
        );

        //Button MyFamily Ketika DiKlik
        btnfmly.setOnClickListener(new View.OnClickListener()
                                   {
                                       @Override
                                       public void onClick(View v)
                                       {
                                           Toast.makeText(getApplicationContext(), "MyFamily Telah Dipilih", Toast.LENGTH_SHORT).show();
                                           Intent beach = new Intent(MainActivity.this, ActivityFamily.class);
                                           startActivity(beach);
                                       }
                                   }
        );

        //Button Quiz Ketika DiKlik
        btnquiz.setOnClickListener(new View.OnClickListener()
                                   {
                                       @Override
                                       public void onClick(View v)
                                       {
                                           Toast.makeText(getApplicationContext(), "Quiz Telah Dipilih", Toast.LENGTH_SHORT).show();
                                           Intent beach = new Intent(MainActivity.this, ActivityQuiz.class);
                                           startActivity(beach);
                                       }
                                   }
        );

        //Button Exit Ketika DiKlik
        btnclose.setOnClickListener(new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            Toast.makeText(getApplicationContext(), "Aplikasi Keluar", Toast.LENGTH_SHORT).show();
                                            finish();
                                            System.exit(0);
                                        }
                                    }
        );


    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();

    }
}